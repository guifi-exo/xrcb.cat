<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package xrcb
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title>XRCB - Xarxa Ràdios Comunitàries de Barcelona<?php //wp_title( '|', true, 'right' ); ?></title>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/MyFontsWebfontsKit.css">

	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
	<script src="<?php echo get_template_directory_uri(); ?>/vendor/leaflet-ajax-geojson.min.js"></script>

	<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/loader.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/map.php" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen" />

</head>

<body <?php body_class(); ?> >
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>

	<header id="masthead" class="site-header" role="banner">
		<nav id="site-navigation" class="navigation-main" role="navigation">
			<h1><a class="logo logo-vectorial" href="<?php echo esc_url( home_url( '/' ) ); ?>"><span class="hide">xrcb.cat</span></a></h1>
			<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'xrcb' ); ?>"><?php _e( 'Skip to content', 'xrcb' ); ?></a></div>
			<!-- begin menu mobile  -->
			<nav class="toggle-mobile-xrcb ">
				<legend class="hide">menu</legend>
				<input id="hide" class="toggle hide" name="toggle" type="radio">
				<label for="show" class="toggle-show"><span class="hide">open</span></label>
				<input id="show" class="toggle hide" name="toggle" type="radio">
				<div class="toggled-menu">
					<label for="hide" class="toggle-hide"><span class="hide">close</span></label>
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				</div>
			</nav>
			<!-- end menu mobile -->
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="main" class="site-main">
	<nav id="secondary-nav" class="nav-secondary nav-over-map nav-mobile-secondary">
		<div id='div_radios' class="mapboxgl-ctrl-group mapboxgl-ctrl">
			<button id="bt_radios" title="radio list view" class="leaflet-btn">radios</button>
		</div>
		<div id='div_podcasts' class="mapboxgl-ctrl-group mapboxgl-ctrl">
			<button id="bt_podcasts" title="podcast list view" class="leaflet-btn">podcasts</button>
		</div>
		<div id='div_all_radios' class="mapboxgl-ctrl-group mapboxgl-ctrl">
			<button id="bt_all_radios" title="show all radios" class="leaflet-btn">all</button>
		</div>
		<div id='div_reset' class="mapboxgl-ctrl-group mapboxgl-ctrl">
			<button id="bt_reset" title="reset map" class="leaflet-btn">reset</button>
		</div>
	</nav>
	<div id="map"></div>
	<div id="mobileplayer"></div>
