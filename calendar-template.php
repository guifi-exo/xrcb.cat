<?php
/**
 * Template Name: Calendario
 *
 * @package xrcb
 */

get_header(); ?>

	<style>
		#script-warning {
			display: none;
			background: #eee;
			border-bottom: 1px solid #ddd;
			padding: 0 10px;
			line-height: 40px;
			text-align: center;
			font-weight: bold;
			font-size: 12px;
			color: red;
		}

		#loading {
			display: none;
			position: absolute;
			top: 10px;
			right: 10px;
		}

		table {
			margin: auto;
		}
	</style>

	<div id="primary" class="content-area" style="width:85%;">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="page-header">
				<h1 class="page-title"><?php the_title(); ?></h1>
				<!--<div><a class="btn-cat" href="<?php //echo get_site_url(); ?>/<?php //echo wpm_get_language(); ?>/llistat-podcasts/">Switch to podcast view</a></div>-->
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/vendor/fullcalendar.min.css">
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/moment.min.js"></script>
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/fullcalendar.min.js"></script>
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/fullcalendar-locale-all.js"></script>

				<div id='script-warning'>
					<code>inc/get-events.php</code> must be running.
				</div>

				<div id='loading'>loading...</div>

				<div id='calendar'></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	    $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,listDay'
			},
			views: {
				listDay: {
					type: 'listDay',
					duration: { days: 1 },
					buttonText: 'Dia'
				}
			},
			//initialView: 'listDay',
			locale: '<?php echo wpm_get_language(); ?>',
			timeFormat: 'HH:mm',
			navLinks: true, // can click day/week names to navigate views
			eventLimit: 4,
			contentHeight: 'auto',
			viewRender: function(view, element) {
				// add handler to watch items inserted into .fc-view to catch popovers and re-init player
				$('.fc-view').on('DOMNodeInserted', function (e) {
					if ($(e.target).hasClass('fc-popover')) {
						bindPlayer();
					}
				});
			},
			events: {
				url: '<?php echo get_stylesheet_directory_uri(); ?>/inc/get-events.php',
				error: function() {
					$('#script-warning').show();
				}
			},
			eventRender: function(event, element) {

				if (!event.empty) {

					if (!event.live) {
						//calendar view
						$(".fc-content", element).prepend("<div class='btn btn-play piwik_download' data-src='"+event.file+"' data-radio='"+event.radio+"' data-title='"+event.title+"' data-radio-link='"+event.radiolink+"' data-podcast-link='"+event.podcastlink+"'></div>");
						// list view
						$(".fc-list-item-title", element).prepend("<div class='btn btn-play piwik_download' data-src='"+event.file+"' data-radio='"+event.radio+"' data-title='"+event.title+"' data-radio-link='"+event.radiolink+"' data-podcast-link='"+event.podcastlink+"'></div>");

						// add class if event was created by algorithm and not by human
						if (event.algorithm) {
							$(".fc-content", element).addClass("algorithm");
							$(".fc-list-item-title", element).parent().addClass("algorithm");
						}
					}
					else {
						// add class if event has flag LIVE
						$(".fc-content", element).addClass("live");
						$(".fc-list-item-title", element).parent().addClass("live");

						// show end time when LIVE event
						if (event.end) {
							let time = event.end._i;
							time = time.substr(time.indexOf("T")+1,5);
							$(".fc-time", element).text($(".fc-time", element).text()+"-"+time);
						}
					}
				} else {
					$(".fc-content", element).addClass("empty");
				}
			},
			eventAfterAllRender: function() {
				bindPlayer();
			},
	    	loading: function(bool) {
	        	$('#loading').toggle(bool);
	    	}
	    });

	    // by default show agenda 
		$('#calendar').fullCalendar('changeView', 'listDay');
	});
</script>

<?php get_footer(); ?>
