
<?php
/**
 * Template Name: Home 2024
 *
 * template to allow choosing between the 
 * institutional web site (ajuntament) and 
 * the Lab web site (we the people)
 *
 * @package xrcb
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  	<meta charset="<?php bloginfo( 'charset' ); ?>" />
  	<meta name="viewport" content="width=device-width" />
  	<title>XRCB - Xarxa Ràdios Comunitàries de Barcelona<?php //wp_title( '|', true, 'right' ); ?></title>
  	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
  	<link rel="profile" href="http://gmpg.org/xfn/11" />
  	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  
  	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/MyFontsWebfontsKit.css">
  
  	<?php //wp_head(); ?>
  	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen" />
    <style>
     @media screen {
       .home-2024 {
         padding: 0;
         margin: 0;
         font-family: HelveticaNeueLTPro-Md, Helvetica, Arial, sans-serif;
       }
       .home-2024 ul {
         position: absolute;
         top:0;
         left:0;
         width: 100vw;
         height: 100vh;
         padding: 0;
         margin: 0;
         display: grid;
         grid: 1fr 1fr / 1fr ;
       }
       .home-2024 li {
         display: grid;
         align-content: center;
	 justify-self: center;
	 width: 100%;
         list-style: none;
         background-color: #FFF;
       }
       .home-2024 li article {
         margin: 0 10vw;
       }
       .home-2024 * {
	 color: #000;
       }
       .home-2024 a:hover,
       .home-2024 a:hover * {
         color: #555;
       }
       .home-2024 li:nth-of-type(even) {
         background-color: black;
       }
       .home-2024 li:nth-of-type(even) * {
         color: #fff;
       }
       .home-2024 li:nth-of-type(even) a:hover,
       .home-2024 li:nth-of-type(even) a:hover * {
         color: #bbb;
       }
       .home-2024 a {
         text-decoration: none;
       }
     }
     @media screen and (min-width: 768px) {
       .home-2024 ul {
          grid: auto / 1fr 1fr;
       }
       .home-2024 li {
         align-content: baseline;
	 padding-top: 20%;
       }
       .home-2024 li a {
	 padding-top: 0;
       } 
       .home-2024 li article a {
         align-self: unset;
       }
     }
     @media screen and (min-width: 1024px) {
       .home-2024 li {
	 padding-top: 33%;
       }
     }
    </style>
  </head>
  <body class="home home-2024">
		<main class="options">
       <ul>
	 <li>
           <article>
             <a href="#">
               <h2>Plataforma Institucional</h2>
               <h3>Xarxa de Radios Comunitarias 
                 <br />
                 de Barcelona</h3>
	     </a>
           </article>
         </li>
         <li>
           <article>
             <a href="https://peertube.xrcb.cat/c/xrcb_channel/videos">
               <h2>Laboratorio Comunitario</h2>
	       <h3>XRCB en construcción</h3>
	     </a>
	     <p>
	       out of order for now, send email to: <a href="mailto:info@xrcb.cat">info@xrcb.cat</a>
	     </p>
	     <p>
	       fuera de servicio por ahora, enviar email a: <a href="mailto:info@xrcb.cat">info@xrcb.cat</a>
	     </p>
           </article>
	 </li>
         </li>
       </ul>
    </main>
  </body>
</html>
