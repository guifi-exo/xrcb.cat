<?php
/**
 * Template Name: podcast statistics
 *
 * @package xrcb
 */

if (!is_user_logged_in())
	wp_redirect(wp_login_url(get_permalink()));

get_header(); ?>

	<style>
		.dataTable td .ratio {
		    color: #999999;
		    display: inline-block;
		    visibility: hidden;
		    text-align: right;
		    margin-right: 4px;
		    font-weight: normal;
		}
	</style>

<script>
	function loadStats(period, date) {
		jQuery.when(
			jQuery.getJSON("https://<?php echo MATOMO_DOMAIN ?>/index.php?module=API&method=Actions.getDownloads&idSite=1&period="+period+"&date="+date+"&format=json&expanded=1&token_auth=4fd9ddb4aaa5da961435fea135184f6d", function(json) {})
	    ).then(function(json) { 

	    	if (json.length > 0) {
		    	//console.log(json[0].subtable);

		    	var html = "";

		    	json[0].subtable.forEach(function(register){
		    		if (register.label != "Others") {
					   	html += "<tr>";
					    html += "<td>"+register.label.substring(19)+"</td>";
					    html += "<td>"+register.nb_visits+"</td>";
					    html += "<td>"+register.nb_hits+"</td>";
					    html += "</tr>";
					}
				});
				jQuery("#tablebody").append(html);

			    jQuery('#download-table').DataTable({
			    	order: [[ 2, "desc" ]],
			    	pageLength: 500,
			    	paging: false,
			    	columnDefs: [
					    { "width": "50%", "targets": 0 }
					]
			    });
			}
	    }
	    ).fail(function(error) {
	    	console.log("error, failed to receive matomo resouce");
	    }) ;
	}
</script>

	<div id="primary" class="content-area stats">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="page-header">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<?php
				// default
				$period = "day";
				$date = "today";

				// get initial parameters
				if (isset($_GET['period'])) {
				    $period = $_GET['period'];
				}
				if (isset($_GET['date'])) {
				    $date = $_GET['date'];
				}

				//echo $period.$date;
			?>

			<div class="entry-content">

				<h2>most listened/downloaded podcasts</h2>

				<button data-period="day" data-date="today" class="btnStats<?php echo (($period=='day'&&$date=='today') ? ' active' : '') ?>">today</button>
				<button data-period="day" data-date="yesterday" class="btnStats<?php echo (($period=='day'&&$date=='yesterday') ? ' active' : '') ?>">yesterday</button>
				<button data-period="week" data-date="today" class="btnStats<?php echo (($period=='week'&&$date=='today') ? ' active' : '') ?>">this week</button>
				<button data-period="month" data-date="today" class="btnStats<?php echo (($period=='month'&&$date=='today') ? ' active' : '') ?>">this month</button>
				<button data-period="year" data-date="today" class="btnStats<?php echo (($period=='year'&&$date=='today') ? ' active' : '') ?>">this year</button>
				<button data-period="range" data-date="2022-01-01,2022-12-31" class="btnStats<?php echo (($period=='range'&&$date=='2022-01-01,2022-12-31') ? ' active' : '') ?>">2022</button>
				<button data-period="range" data-date="2021-01-01,2021-12-31" class="btnStats<?php echo (($period=='range'&&$date=='2021-01-01,2021-12-31') ? ' active' : '') ?>">2021</button>
				<button data-period="range" data-date="2020-01-01,2020-12-31" class="btnStats<?php echo (($period=='range'&&$date=='2020-01-01,2020-12-31') ? ' active' : '') ?>">2020</button>
				<button data-period="range" data-date="2019-01-01,2019-12-31" class="btnStats<?php echo (($period=='range'&&$date=='2019-01-01,2019-12-31') ? ' active' : '') ?>">2019</button>
				<button data-period="range" data-date="2018-10-05,2018-12-31" class="btnStats<?php echo (($period=='range'&&$date=='2018-10-05,2018-12-31') ? ' active' : '') ?>">2018</button>
				<button data-period="range" data-date="2018-10-05,today" class="btnStats<?php echo (($period=='range'&&$date=='2018-10-05,today') ? ' active' : '') ?>">total</button>

				<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.js"></script>

				<div id="datatable">

					<table id="download-table">
						<thead>
							<tr class="level0">
								<th class="sortable first label" id="label">
									<div id="thDIV" class="thDIV">URL</div>
								</th>
								<th class="sortable  columnSorted" id="nb_visits">
									<div class="columnDocumentation">
										<div class="columnDocumentationTitle">
											<span class="icon-help"></span>
											unique listenings
										</div>
									</div>
									<div id="thDIV" class="thDIV"><span class="sortIcon desc " width="16" height="16"></span><span class="sortIcon desc " width="16" height="16"></span></div>
								</th>
								<th class="sortable last" id="nb_hits">
									<div class="columnDocumentation">
										<div class="columnDocumentationTitle">
											<span class="icon-help"></span>
											total listenings
										</div>
									</div>
								</th>
							</tr>
						</thead>
						<tbody id="tablebody">

						<script>
							// preload program filter
							let params = new URLSearchParams(document.location.search.substring(1));
							let period = params.get("period");
							let date = params.get("date");

							if (period === null || date === null) {
								period = "day";
								date = "today";
							}

							//console.log(params, period, date);

							loadStats(period, date);
						</script>

						</tbody>
					</table>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	    /*$('#download-table tbody')
        .on( 'mouseenter', 'td', function () {
            var colIdx = table.cell(this).index().column;
 
            $( table.cells().nodes() ).removeClass( 'highlight' );
            $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
        } );*/

        $(".btnStats").click(function() {
			//console.log($(this).data("period"), $(this).data("date"));

			$(".btnStats").removeClass("active");
			$(this).addClass("active");

			jQuery("#tablebody").empty();

			jQuery('#download-table').DataTable().clear().destroy();

			loadStats($(this).data("period"), $(this).data("date"));
		});
	});
</script>

<?php get_footer(); ?>
