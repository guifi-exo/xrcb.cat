jQuery(document).ready(function($) {
	// change menu name Easy Appointment to Programació
	$('#toplevel_page_easy_app_top_level .wp-menu-name').text("Programació");

	// overwrite link to "Add Podcast" left menu
	if ($('#menu-posts-podcast .wp-submenu li:nth-of-type(3) a').attr('href').indexOf('post-new.php?post_type=podcast') !== -1) {
		$('#menu-posts-podcast .wp-submenu li:nth-of-type(3) a').attr('href', 'https://xrclab.cat/ca/publish-podcast/');
	}

	// overwrite link to "Add Podcast" top menu
	$('#wp-admin-bar-new-content .ab-sub-wrapper li a').each(function() {
		if ($(this).attr('href').indexOf('post-new.php?post_type=podcast') !== -1) {
			$(this).attr('href', 'https://xrclab.cat/ca/publish-podcast/');
		}
	});
});

