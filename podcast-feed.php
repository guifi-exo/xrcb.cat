<?php
/**
	Template Name: Podcast FEED
*/

header("Content-Type: application/rss+xml; charset=UTF-8");
$fp = fopen('php://output', 'w');

if (isset($_GET['id'])) {
	$radio_id = $_GET['id'];
	$radio = get_post( $radio_id );

	$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
    $rssfeed .= '<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">'.PHP_EOL;
    $rssfeed .= '<channel>'.PHP_EOL;
    $rssfeed .= '<title>'.(isset($_GET['id_program']) ? get_term($_GET['id_program'])->name.' - ' : '').$radio->post_title.'</title>'.PHP_EOL;
    $rssfeed .= '<link>'.get_permalink( $radio_id ).'</link>'.PHP_EOL;

    $lang = get_post_meta($radio_id, 'idioma_podcast', true);
    if ($lang == '' || $lang == 'ca') {
        $ext = '';
        $lang = 'ca';
    }
    else $ext = '_'.$lang;
    $rssfeed .= '<description>'.get_post_meta($radio_id, 'historia'.$ext, true).'</description>'.PHP_EOL;
	$rssfeed .= '<author>'.$radio->post_title.'</author>'.PHP_EOL;
    $rssfeed .= '<language>' . $lang . '</language>'.PHP_EOL;
    $rssfeed .= '<copyright>Copyleft (CC) 2023 xrcb.cat</copyright>'.PHP_EOL;

    $rssfeed .= '<itunes:subtitle>XRCB</itunes:subtitle>'.PHP_EOL;
	$rssfeed .= '<itunes:author>XRCB - '.$radio->post_title.'</itunes:author>'.PHP_EOL;
	$rssfeed .= '<itunes:summary></itunes:summary>'.PHP_EOL;
	$rssfeed .= '<itunes:owner>'.PHP_EOL;
	$rssfeed .= '<itunes:email>'.$radio->mail.'</itunes:email>'.PHP_EOL;
	$rssfeed .= '</itunes:owner>'.PHP_EOL;

    $img = wp_get_attachment_url(get_post_meta($radio_id, 'img_podcast', true));
    if ($img == false)
        $img = get_stylesheet_directory_uri()."/images/Logo-podcasts.jpg";
	$rssfeed .= '<itunes:image href="' . $img . '" />'.PHP_EOL;
	$rssfeed .= '<itunes:explicit>clean</itunes:explicit>'.PHP_EOL;
	$rssfeed .= '<itunes:keywords></itunes:keywords>'.PHP_EOL.PHP_EOL;

    $podcast_query = array(
        'posts_per_page' => '-1',
        'post_type' => 'podcast',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'     => 'radio',
                'value'   => $radio_id,
                'compare' => '=',
            ),
            array(
                'relation' => 'OR',
                array(
                    'key'     => 'live',
                    'compare' => 'NOT EXISTS',
                ),
                array(
                    'key'     => 'live',
                    'value'   => 'true',
                    'compare' => '!=',
                ),
            ),
        ),
    );

    // limitar subscripción a un programa
    if (isset($_GET['id_program'])) {
        $podcast_query['tax_query'] = array(
            array(
                'taxonomy'  => 'podcast_programa',
                'terms'     => $_GET['id_program'],
            ),
        );
    }
    $podcast_posts = new WP_Query($podcast_query);

    // get reposts
    $repost_query = array(
        'posts_per_page' => '-1',
        'post_type' => 'repost',
        'author'   => get_post_field('post_author', $radio_id),
    );
    $repost_posts = new WP_Query($repost_query);

    // merge podcasts and reposts
    $result_podcast_repost = new WP_Query();
    $result_podcast_repost->posts = array_merge( $podcast_posts->posts, $repost_posts->posts );
    $result_podcast_repost->posts = wp_list_sort( $result_podcast_repost->posts, 'post_date', 'DESC' );
    $result_podcast_repost->post_count = count( $result_podcast_repost->posts );

    while($result_podcast_repost->have_posts()) : $result_podcast_repost->the_post();

		$rssfeed .= '<item>'.PHP_EOL;
        $rssfeed .= '<title>' . get_the_title() . '</title>'.PHP_EOL;
        $rssfeed .= '<itunes:summary>' . htmlspecialchars(get_the_content()) . '</itunes:summary>'.PHP_EOL;
        $rssfeed .= '<description>' . htmlspecialchars(get_the_content()) . '</description>'.PHP_EOL;

        if (get_post_type(get_the_ID()) == 'podcast') {
            $id = get_the_ID();
            $permalink = get_the_permalink();
        } else {
            $id = get_field('podcast')->ID;
            $permalink = get_post_permalink($id);
        }

        $mp3_meta = wp_get_attachment_metadata(get_post_meta($id, 'file_mp3', true));
        $mp3_meta_filesize = "";
        if (isset($mp3_meta['filesize'])) $mp3_meta_filesize = $mp3_meta['filesize'];
        $mp3_meta_mime_type = "";
        if (isset($mp3_meta['mime_type'])) $mp3_meta_mime_type = $mp3_meta['mime_type'];
        $rssfeed .= '<enclosure url="' . wp_get_attachment_url(get_post_meta($id, 'file_mp3', true)) . '" length="' . $mp3_meta_filesize . '" type="' . $mp3_meta_mime_type . '" />'.PHP_EOL;

        $rssfeed .= '<guid isPermaLink="false">' . get_the_guid() . '</guid>'.PHP_EOL;
        $rssfeed .= '<link>' . $permalink . '</link>'.PHP_EOL;
        $rssfeed .= '<pubDate>' . get_post_time(DATE_RSS, false, $id, false) . '</pubDate>'.PHP_EOL;
        $rssfeed .= '</item>'.PHP_EOL.PHP_EOL;
    
    endwhile;

    $rssfeed .= '</channel>'.PHP_EOL;
    $rssfeed .= '</rss>'.PHP_EOL;
 
    echo $rssfeed;
}

fclose($fp);

?>