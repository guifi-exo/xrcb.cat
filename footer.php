<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package xrcb
 */
?>

	</div><!-- #main -->

	<?php
	// check player plugin
	if( function_exists( 'xrcbplayer_show_player' ) ) {
    	xrcbplayer_show_player();
	}
	?>

</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	    // make active menu bold
		$('#menu-principal a').click(function() {
			$('#menu-principal li').removeClass('current_page_item');
			$(this).parent().addClass('current_page_item');
		});

		// add language menu
		var path = window.location.pathname.replace("/"+document.documentElement.lang+"/", "");

		$('.menu-lang').empty();
		$('.menu-lang').append('<ul class="wpm-language-switcher switcher-list"><li class="item-language-ca <?php echo (wpm_get_language()=="ca") ? "active" : ""; ?>"><a href="/ca/'+path+'" data-lang="ca">ca</a></li><li class="item-language-es <?php echo (wpm_get_language()=="es") ? "active" : ""; ?>"><a href="/es/'+path+'" data-lang="es">es</a></li><li class="item-language-en <?php echo (wpm_get_language()=="en") ? "active" : ""; ?>"><a href="/en/'+path+'" data-lang="es">en</a></li></ul>');

		// activate broadcast button
		$(".broadcast-btn > a").click(function() {
			playBroadcast();
			$(".xrcbplayer .player").removeClass("playing-off");
			$(".xrcbplayer .player").addClass("playing-on");
			$(".xrcbplayer .player").removeClass("src-podcast");
			$(".xrcbplayer .player").addClass("src-stream");
		});

		// close mobile menu on click
		$(".menu-item a").click(function() {
			$( "#hide" ).prop( "checked", true );
		});
	});
</script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/kiwiirc-mods.css">

</body>
</html>
