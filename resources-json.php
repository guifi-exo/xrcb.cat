<?php
/**
	Template Name: Resources JSON
*/

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

$my_query = new WP_Query('post_type=resource&post_status=publish&posts_per_page=100&order=ASC&orderby=ID');

if ( $my_query->have_posts() ) {

	$data = array();

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$files_mp3 = [];
		$files_pdf = [];

		$attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_parent' => $post->ID,
            'exclude'     => get_post_thumbnail_id()
        ) );
 
        if ( $attachments ) {
            foreach ( $attachments as $attachment ) {
                if ( $attachment->post_mime_type == 'audio/mpeg' ) {
					$files_mp3[] = array(
						"title" => wp_get_attachment_metadata( $attachment->ID )["title"],
						"url" => wp_get_attachment_url( $attachment->ID ),
						//"meta" => wp_get_attachment_metadata( $attachment->ID ),
					);    
	            }
	            /*else if ( $attachment->post_mime_type == 'application/pdf' ) {
					$files_pdf[] = array(
						"title" => wp_get_attachment_metadata( $attachment->ID )["title"],
						"url" => wp_get_attachment_url( $attachment->ID ),
						//"meta" => wp_get_attachment_metadata( $attachment->ID ),
					);    
	            }*/
            }
        }

		$data[] = array(
			"id" => $post->ID,
			"title" => get_the_title(),
			"description" => get_the_content(),
			"excerpt" => get_the_excerpt(),
			"author" => get_the_author(),
			"files_mp3" => $files_mp3,
			//"files_pdf" => $files_pdf,
			"url" => get_post_meta(get_the_ID(), 'url', true),
			"permalink" => get_permalink(get_the_ID()),
		);
	}

	echo json_encode(array("data" => $data));
}

fclose($fp);

?>