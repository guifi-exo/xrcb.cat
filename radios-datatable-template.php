<?php
/**
 * Template Name: Radios Datatable
 *
 * @package xrcb
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.js"></script>

				<div id="datatable"></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="radio-table"></table>');
		var datatable = $('#radio-table').DataTable({
			paging: false,
			info: false,
			responsible: true,
			search: true,
			stateSave: true,
			ajax: { url :"<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/index.php/radios-json/", type : "GET"},
			columns: [
				{ "data": "title", "title" : "Nom", "className" : "name", "render": function ( data, type, row ) { return "<a class='btn-radio' href='"+row["permalink"]+"' data-lat='"+row["lat"]+"' data-lon='"+row["lon"]+"'>"+data+"</a>"; }, },
				{ "data": "categoria", "title" : "Categoria", "className" : "category"},
				{ "data": "barrio", "title" : "Barri", "className" : "neighbourhood"},
				{ "data": "year", "title" : "Año fundación", "className" : "year"},
				{ "data": "licencia", "title" : "Licencia", "className" : "license"},
				{ "data": "web", "title" : "URL", "className" : "url", "render": function ( data, type, row ) { return "<a target='_blank' href='"+data+"'>"+data+"</a>"; }, },
			],
		}).on( 'init.dt', function () {

			// show radios count
			$("#radio-table_wrapper").prepend("<div class='dataTables_length radios-count'><span class='recordsDisplay'>"+datatable.page.info()["recordsDisplay"]+"</span> / "+datatable.page.info()["recordsTotal"]+" Radios</div>");

		    $("a.btn-radio").click(function(){
		        globalmap.flyTo(new L.LatLng($(this).data("lat"), $(this).data("lon")), 16, false);
		    });
		}).on( 'search.dt', function () {
			// modificar podcast count
			$("#radio-table_wrapper .recordsDisplay").text(datatable.page.info()["recordsDisplay"]);
		});

	});
</script>

<?php get_footer(); ?>
