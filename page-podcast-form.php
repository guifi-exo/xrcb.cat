<?php
/**
 * Template Name: Podcast form
 *
 * Front end form to submit podcasts
 * @package xrcb
 */

if (!is_user_logged_in() || current_user_can('contributor'))
	wp_redirect(wp_login_url(get_permalink()));

// only for non administrators
if (!current_user_can('manage_options')) {

	// get radios for this user
	$meta_query = new WP_Query( array(
		'author' => get_current_user_id(),
		'post_type' => 'radio',
		'post_status' => 'publish',
	));
	$radios = $meta_query->get_posts();
	if (count($radios) < 1)
		wp_redirect(home_url());
}

// get GET parameter for editing existing post
if (isset($_GET['post_id']) && is_numeric($_GET['post_id']) ) {
	$post_id = $_GET['post_id'];
}

acf_form_head();

get_header(); ?>

	<div id="primary" class="content-area page">
		<div id="content" class="site-content" role="main">
		<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>
		 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			  <header class="entry-header">
				<h1 class="entry-title">
					<?php if (isset($post_id)) {
						echo "Edita podcast";
					} else {
						the_title();
					}
					?>
				</h1>
			</header><!-- .entry-header -->

			  <div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php

					$fields = array(
				    	'etiquetes',
				    	'idioma',
				    	'description',
				    	'file_mp3',
				    	'img_podcast',
				    	'privacidad',
				    	'ipsave_podcast'
				    );

			    	// check if user has programs
					$programs = get_terms(array(
						'taxonomy' => 'podcast_programa',
						'hide_empty' => false,
						'meta_key' => 'usuari',
					    'meta_value' => get_current_user_id(),
					));

					// hide program field
					if (current_user_can('manage_options') || count($programs) > 0) {
					    array_unshift($fields, 'programes');
					}

					array_unshift($fields, 'radio');

					$options = array(
					    'fields' => $fields,
					    'post_title' => true,
					    'post_content' => false,
					    'uploader' => 'basic',
					    'submit_value' => __('Llest!', 'xrcb'),
					    'return' => '%post_url%',
					);

					if (isset($post_id)) {
						// edit existing podcast
						$options['post_id'] = $post_id;
					}
					else {
						// create new podcast
						$options['post_id'] = 'new_post';
					    $options['new_post'] = array(
				            'post_type' => 'podcast',
				            'post_status' => 'publish'
				        );
					}
					acf_form( $options );

					// hide radio select if only one radio station
					if (!current_user_can('manage_options') && count($radios) == 1) : ?>
						<script>jQuery(".acf-field-5b2a14e0b2380").hide();</script>
					<?php
					endif;
					?>

				<?php endwhile; ?>

			</div>
		  </article>
		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
(function($) {

	acf.add_filter('select2_args', function( args, $select, settings ){
		// limit to 3
		args.maximumSelectionLength = 3;
		return args;
	});

})(jQuery);
</script>

<?php get_footer(); ?>
