<?php
/**
*	Template Name: Radios GeoJSON CV
*/

header('Content-Type: application/json; charset=utf-8');
//header('Content-Disposition: attachment; filename="export.csv"');
$fp = fopen('php://output', 'w');

$my_query = new WP_Query('post_type=radio&post_status=publish&posts_per_page=100&order=ASC&orderby=ID');

if ( $my_query->have_posts() ) {

	$features = array(
		'features' => array(),
		'type' => 'FeatureCollection'
	);

	while ($my_query->have_posts()) {

		$my_query->the_post();

		$location = get_post_meta(get_the_ID(), 'location', true);

		$thumb_id = get_post_thumbnail_id(get_the_ID());
		$thumb_url = wp_get_attachment_image_src($thumb_id,'medium', true)[0];

		if ($location) {

			$features['features'][] = array(
				'id' => (int)get_the_ID(),
				'properties' => array(
					'id' => (int)get_the_ID(),
					"title" => get_the_title(),
					"categoria" => get_post_meta(get_the_ID(), 'categoria', true),
					"barrio" => get_post_meta(get_the_ID(), 'barrio', true),
					"address" => $location["address"],
					"web" => get_post_meta(get_the_ID(), 'web', true),
					"permalink" => get_permalink(get_the_ID()),
					"historia" => get_post_meta(get_the_ID(), 'historia', true),
					"historia_es" => get_post_meta(get_the_ID(), 'historia_es', true),
					"historia_en" => get_post_meta(get_the_ID(), 'historia_en', true),
					"licencia" => get_post_meta(get_the_ID(), 'licencia', true),
					"anyo_fundacion" => get_post_meta(get_the_ID(), 'anyo_fundacion', true),
					"img" => $thumb_url
				),
				'type' => 'Feature',
				'geometry' => array(
					'type' => 'Point',
					'coordinates' => array(
						(float)$location["lng"],
						(float)$location["lat"]
					)
				)
			);
		}
	}

	echo json_encode($features);
}

fclose($fp);

?>
