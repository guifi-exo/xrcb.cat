<?php
/**
* Template Name: Radio form
*
* Front end form to submit radios
* @package xrcb
*/

if (!is_user_logged_in())
wp_redirect(home_url());

// get GET parameter for editing existing post
if (isset($_GET['post_id']) && is_numeric($_GET['post_id']) ) {
	$post_id = $_GET['post_id'];
}

acf_form_head();

get_header(); ?>

<div id="primary" class="content-area page">
	<div id="content" class="site-content" role="main">
		<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<h1 class="entry-title">
					<?php if (isset($post_id)) {
						echo "Edita radio";
					} else {
						the_title();
					}
					?>
				</h1>
			</header><!-- .entry-header -->
			<div class="entry-content">

				<p>Bienvendios a la comunidad XRCB. Estas son <a href="https://preprod.xrcb.cat/es/qui-som/comunitat/">nuestras bases</a>, sientate en casa!</p>
				<p>Si necesitas ayuda para rellenar el formulario, escribenos a <a href="mailto:suport@xrcb.cat">suport@xrcb.cat</a></p>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php

					$fields = array(
						'ciudad',
						'barrio',
						'location',
						'categories',
						'web',
						'anyo_fundacion',
						'licencia',
						'historia',
						'historia_es',
						'historia_en',
						'fm',
						'img_podcast',
						'twitter',
						'instagram',
						'imatges_gif_animat',
						'buenaspracticas',
						'privacidad',
					);

					$options = array(
						'fields' => $fields,
						'post_title' => true,
						'post_content' => false,
						'uploader' => 'basic',
						'submit_value' => __('Llest!', 'xrcb'),
						'return' => '%post_url%',
					);

					if (isset($post_id)) {
						// edit existing podcast
						$options['post_id'] = $post_id;
					}
					else {
						// create new podcast
						$options['post_id'] = 'new_post';
						$options['new_post'] = array(
							'post_type' => 'radio',
							'post_status' => 'publish'
						);
					}
					acf_form( $options );

				endwhile; ?>

				<p>Para complementar el proceso de alta de una radio nueva hace falta que <a href="https://xrclab.cat/ca/publish-podcast/">publiques 5 podcasts</a>. Los podcast serán revisados por el team y te daremos feedback en caso necesario.</p>

			</div>
		</article>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>
