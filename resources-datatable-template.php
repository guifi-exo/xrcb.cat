<?php
/**
 * Template Name: Resources Datatable
 *
 * @package xrcb
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>

				<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.css">
				<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/vendor/jquery.dataTables.min.js"></script>

				<div id="datatable"></div>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="resources-table"></table>');
		$('#resources-table').dataTable({
			paging: false,
			info: false,
			responsible: true,
			search: true,
			stateSave: true,
			ajax: { url :"<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/index.php/resources-json/", type : "GET"},
			columns: [
				{ "data": "title", "title" : "Nom i enllaç", "className" : "name", "render": function ( data, type, row ) { if (row["url"] !== "") return "<a target='_blank' href='"+row["url"]+"'>"+data+"</a>"; else return "<a href='"+row["permalink"]+"'>"+data+"</a>"; }, },
				{ "data": "description", "title" : "Descripció", "className" : "description", "render": function ( data, type, row ) { if (row["excerpt"] !== "") return row["excerpt"]; else return data; }, },
				{ "data": "author", "title" : "Autor", "className" : "author"/*, "render": function ( data, type, row ) { return "<a href='<?php //echo get_site_url(); ?>/<?php //echo wpm_get_language(); ?>/index.php/author/"+data+"'>"+data+"</a>"; }*/ },
				{ "data": "files_mp3", "title" : "MP3s", "className" : "mp3s", "render": function ( data, type, row ) { var files=""; data.forEach(function(file) { files += "<span class='btn btn-play piwik_download' data-src='"+file.url+"' data-radio='"+row['author']+"' data-title='"+file.title+"' data-radio-link='"+row["permalink"]+"' data-podcast-link='"+row["permalink"]+"'></span>"+file.title+"<br />"; }); return files; }, 
				},
				/*{ "data": "files_pdf", "title" : "Altres", "render": function ( data, type, row ) { var files=""; data.forEach(function(file) { files += "<a href='"+file.url+"'>PDF</a>"; }); return files; }, 
				},*/
			],
		}).on( 'init.dt', function () {
			bindPlayer();
		});
	});
</script>

<?php get_footer(); ?>
