<?php
    //Let's set the header straight
    header('Content-type: text/javascript');

    //Get the WP-specifics, so that we can use constants and what not
    $home_dir = preg_replace('^wp-content/themes/[a-z0-9\-/]+^', '', getcwd());
    include($home_dir . 'wp-load.php');
?>

globalmap = null;

var markerPos = 0, 
    markerOldPos = 1,
    markerActive = null,
    markers=[],
    loopInterval = 0,
    tmpAudioRadio = "",
    tmpAudioTitle = "";

function loadMap() {
    var baseUrl = 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png',
        base = (!baseUrl && window.MQ) ? MQ.mapLayer() : L.tileLayer(baseUrl, { 
           subdomains: 'abc'
        }),
        options = L.Util.extend({}, {
            maxZoom: 18,
            minZoom: 11,
            layers: [base],
            zoomControl: 1,
            scrollWheelZoom: 0,
            doubleClickZoom: 1,
            //attributionControl: false
        }, {});
    globalmap = L.map('map', options).setView([41.3990,2.1553],13);
    if (0) {
        globalmap.fit_markers = true;
    }
    var attControl = L.control.attribution({prefix:false}).addTo(globalmap);
		attControl.addAttribution('<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>');
		attControl.addAttribution('© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, © <a href="https://carto.com/about-carto/">CARTO</a>');

    function markerOnClick(e) {
        markerActive = e.target._leaflet_id;

        var link = document.createElement('a');
        link.href = e.target.feature.properties.permalink;
        document.body.appendChild(link);
        link.click();

        // stop loop and hide labels
        clearInterval(loopInterval);
        loopInterval = 0;
        showMarkers(e);

        // show selected label
        window.setTimeout(function() {
            e.target.fire("mouseover");
        }, 2000);

        globalmap.flyTo(e.latlng, 16, false);
    }

    function showMarkers(e) {
        markers.forEach(function(marker){
            if (marker._leaflet_id == markerActive) {
                e.target._tooltip.options.permanent = true;
            } else {
                marker.fire("mouseout");
                e.target._tooltip.options.permanent = false;
            }
        });
    }

    var xrcbIcon1 = new L.Icon({
        iconSize: [30, 30],
        popupAnchor:  [1, -24],
        iconUrl: '<?php echo get_template_directory_uri() ?>/images/marker.png'
    });
    var xrcbIcon2 = new L.Icon({
        iconSize: [25, 25],
        popupAnchor:  [1, -24],
        iconUrl: '<?php echo get_template_directory_uri() ?>/images/marker.png'
    });
    var xrcbIcon3 = new L.Icon({
        iconSize: [20, 20],
        popupAnchor:  [1, -24],
        iconUrl: '<?php echo get_template_directory_uri() ?>/images/marker.png'
    });
    var xrcbIcon4 = new L.Icon({
        iconSize: [15, 15],
        popupAnchor:  [1, -24],
        iconUrl: '<?php echo get_template_directory_uri() ?>/images/marker.png'
    });

	var src = '<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/radios-geojson/',
        default_style = [],
        rewrite_keys = {
            fill : 'fillColor',
            'fill-opacity' : 'fillOpacity',
            stroke : 'color',
            'stroke-opacity' : 'opacity',
            'stroke-width' : 'width',
        },
        layer = L.ajaxGeoJson(src, {
            type: 'json',
            pointToLayer: function(feature, latlng) {
                
                var marker = L.marker(latlng, {icon: xrcbIcon3});
                marker.radioId = feature.properties.id;

                // get last podcast from this radio
                jQuery.getJSON("<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/podcasts-json?radio_id="+feature.properties.id, {})
                .done (function( json ) {
                    if (json && json.hasOwnProperty("data")) {
                        if (json.data.length > 0) {
                            var podcast = json.data[json.data.length-1];
                            marker.podcastUrl = podcast.file_mp3;
                            marker.radioTitle = podcast.radio_name;
                            marker.podcastTitle = podcast.title;
                            marker.podcastLink = podcast.permalink;
                            marker.radioLink = podcast.radio_permalink;
                            //console.log("podcastUrl for", marker.radioId, marker.podcastUrl);
                        }
                    }
                })
                .fail(function(data) {    
                    console.log("json error in podcasts");
                });

                marker.bindTooltip("<a href='"+feature.properties.permalink+"'>"+feature.properties.title+"</a></br><span class='sub'>/ "+feature.properties.categoria+"</span>", {
                    permanent: false, 
                    offset: [10, 0],
                    direction: 'right',
                    interactive: true
                });

                marker.on('click', markerOnClick);

                markers.push(marker);
                return marker;
            },
        }),
        fitbounds = 0;
    if (fitbounds) {
        layer.on('ready', function () {
            this.globalmap.fitBounds( this.getBounds() );
        });
    }
    layer.addTo( globalmap );

    // change icon size
    globalmap.on('zoomend', function() {
        if (globalmap.getZoom() > 16) {
            layer.eachLayer(function(layer) {
                return layer.setIcon(xrcbIcon1);
            });
        } else if (globalmap.getZoom() > 14) {
            layer.eachLayer(function(layer) {
                return layer.setIcon(xrcbIcon2);
            });
        } else if (globalmap.getZoom() > 12) {
            layer.eachLayer(function(layer) {
                return layer.setIcon(xrcbIcon3);
            });
        } else {
            layer.eachLayer(function(layer) {
                return layer.setIcon(xrcbIcon4);
            });
        }
    });

    // reset map
    var btn_reset = document.getElementById('bt_reset');
    btn_reset.onclick = resetMap;

    function resetMap() {
        //globalmap =
        globalmap.setView([41.3990,2.1553],13);
    }

    // turn on/off loop
    var btn_all = document.getElementById('bt_all_radios');
    btn_all.onclick = setMarkerLoop;

    function setMarkerLoop() {
        if (loopInterval === 0) {
            btn_all.innerHTML = 'all';

            // hide all labels
            markers.forEach(function(marker){
                if (marker._leaflet_id !== markerActive) {
                    marker.fire("mouseout");
                }
            });

            loopInterval = window.setInterval(function() {
                //console.log(markerPos, markers);

                // show label
                if (markers && markers[markerPos]) {
                    markers[markerPos].fire("mouseover");
                    if (markers[markerPos]._leaflet_id !== markerActive) {
                        markers[markerOldPos].fire("mouseout");
                    }
                }

                markerOldPos = markerPos;
                markerPos++;
                if (markerPos >= markers.length) markerPos = 0;
            }, 2000);
        }
        else {
            clearInterval(loopInterval);
            loopInterval = 0;

            btn_all.innerHTML = 'loop';

            // show all labels
            markers.forEach(function(marker){
                marker.fire("mouseover");
                //marker.fire("mouseout");
            });
        }
    }

    setMarkerLoop();
}

function bindButtons() {
    // switch to radio list view
    var btn_radios = document.getElementById('bt_radios');
    function showRadioList() {
        var link = document.createElement('a');
        link.href = "<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/llistat-radios/";
        document.body.appendChild(link);
        link.click();
    };
    btn_radios.onclick = showRadioList;

    // switch to podcast list view
    var btn_podcasts = document.getElementById('bt_podcasts');
    function showPodcastList() {
        var link = document.createElement('a');
        link.href = "<?php echo get_site_url(); ?>/<?php echo wpm_get_language(); ?>/llistat-podcasts/";
        document.body.appendChild(link);
        link.click();
    };
    btn_podcasts.onclick = showPodcastList;
}