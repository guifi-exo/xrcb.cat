<?php
/**
	Template Name: Programació Dema JSON

	Gets all appointments and associated podcast data from DB
*/

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

$params = array();

$query_programacio = $wpdb->prepare("SELECT id,date,start
	FROM wp_ea_appointments
	WHERE date = CURDATE()+1
	ORDER BY date ASC,start",
	$params
);
$programacio_tmp = $wpdb->get_results($query_programacio);

//echo json_encode(array("data" => $programacio));

$i = 0;
$programacio = [];
foreach ($programacio_tmp as $prog) {

	// get related podcast of this programming
	$query_podcast = $wpdb->prepare("SELECT value
		FROM wp_ea_fields
		WHERE app_id=$prog->id AND field_id=5",
		$params
	);
	$podcast_id = (int)$wpdb->get_results($query_podcast)[0]->value;

	// only if podcast belongs to radio station
	$radio_id = get_post_meta($podcast_id, 'radio', true);
	if ($radio_id) {
		$programacio[] = $prog;

		$programacio[$i]->title = get_post($podcast_id)->post_title;
		$programacio[$i]->radio_id = $radio_id;
		$programacio[$i]->author_id = get_post($podcast_id)->post_author;
		$programacio[$i]->author_name = get_the_author_meta('display_name', $programacio[$i]->author_id);

		$live = get_post_meta($podcast_id, 'live', true);
		if ($live == "" || $live == "false") {
			$live = false;
		}
		else if ($live == "true") {
			$live = true;
			// get duration from field as for live podcasts there is no related mp3 file
			$programacio[$i]->live_duration = (int)get_post_meta($podcast_id, 'live_duration', true);
		}
		$programacio[$i]->live = $live;

		$post_meta = get_post_meta($podcast_id, 'file_mp3', true);

		$programacio[$i]->audio = [
			"id" => $podcast_id,
			"url" => wp_get_attachment_url($post_meta),
			"meta" => wp_get_attachment_metadata($post_meta)
		];

		$i++;
	}
}

echo json_encode(array("podcasts" => $programacio));

fclose($fp);

?>
