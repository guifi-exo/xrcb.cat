<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package xrcb
 */

get_header(); ?>

<?php 
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

			<header class="page-header">
				<h1 class="page-title">
					<?php echo $curauth->nickname; ?>
				</h1>
			</header><!-- .page-header -->

			<div class="entry-content">
				<p><?php echo $curauth->description; ?></p>

				<h3 class="resources">Recursos publicados</h3>

				<table class="author-resources">

				<?php
				    $resource_query = array(
				    	'posts_per_page' => '-1', 
				    	'post_type' => 'resource', 
				    	'orderby' => 'date',
				    	'order' => 'DESC',
				    	'author' => $curauth->ID,
				    );
				    $resource_posts = new WP_Query($resource_query);
				    
				    while($resource_posts->have_posts()) : $resource_posts->the_post();
				    ?>

			    	<tr>
			        	<td><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></td>
					</tr>

				<?php endwhile; ?>

				</table>
			</div>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>
