<?php
/**
	Template Name: Broadcast JSON

	Gets information about the actual playing podcast at live stream (from calendar)
*/

header('Content-Type: application/json; charset=utf-8');
$fp = fopen('php://output', 'w');

$params = array();

$query_playing = $wpdb->prepare("SELECT id,date,start,end
	FROM wp_ea_appointments
	WHERE date = CURDATE() AND start < CURTIME() AND end > CURTIME()",
	$params
);
$playing = $wpdb->get_results($query_playing);

if (count($playing) > 0) {
	$playing = $playing[0];

	// get podcast id
	$query_podcast = $wpdb->prepare("SELECT value
		FROM wp_ea_fields
		WHERE app_id=$playing->id AND field_id=5",
		$params
	);
	$podcast_id = (int)$wpdb->get_results($query_podcast)[0]->value;

	if ($podcast_id) {

		// get podcast data
		$playing->title = get_post($podcast_id)->post_title;
		$playing->description = get_post($podcast_id)->post_content;
		$playing->radio_id = get_post_meta($podcast_id, 'radio', true);
		$playing->radio_name = get_the_title(get_post_meta($podcast_id, 'radio', true));
		$playing->radio_link = get_permalink(get_post_meta($podcast_id, 'radio', true));
		$playing->link = get_permalink($podcast_id);
		$playing->live = get_post_meta($podcast_id, 'live', true)=="true";
	}
	else {
		// get live data

		// title field
		$query_title = $wpdb->prepare("SELECT value
			FROM wp_ea_fields
			WHERE app_id=$playing->id AND field_id=11",
			$params
		);
		$title = $wpdb->get_results($query_title)[0]->value;

		$playing->title = $title;
		$playing->live = true;

		$playing->file = false;
		$playing->radio = false;
		$playing->radiolink = false;
		$playing->podcastlink = false;
		$playing->algorithm = false;
	}

	echo json_encode(array("data" => $playing));
}
else {
	echo json_encode(array("data" => null));
}

fclose($fp);

?>