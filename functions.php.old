<?php
/**
 * xrcb functions and definitions
 *
 * @package xrcb
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

if ( ! function_exists( 'xrcb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function xrcb_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on xrcb, use a find and replace
	 * to change 'xrcb' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'xrcb', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'xrcb' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // xrcb_setup
add_action( 'after_setup_theme', 'xrcb_setup' );

/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function xrcb_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'xrcb_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'xrcb_register_custom_background' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function xrcb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'xrcb' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'xrcb_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function xrcb_scripts() {
	/* load scripts for jquery tabs */
	if ( !is_admin() ) {
		wp_register_style( 'tabs_css', get_template_directory_uri().'/vendor/jquery-ui.min.css' );
		wp_enqueue_style( 'tabs_css' );
		wp_enqueue_script('jquery-ui-tabs');
	}

	wp_enqueue_style( 'xrcb-style', get_stylesheet_uri() );

	wp_enqueue_script( 'xrcb-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'xrcb-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'xrcb-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	/* ajaxifying theme */
	if ( ! is_admin() ) {
		$url = get_stylesheet_directory_uri() . '/vendor/';
		wp_enqueue_script( 'hash-change', "{$url}jquery.ba-hashchange.min.js", array('jquery'), '', true);
		$url = get_stylesheet_directory_uri() . '/js/';
		wp_enqueue_script( 'ajax-theme', "{$url}ajax.js", array( 'hash-change' ), '', true);
	}

	if (is_admin()) {
		wp_enqueue_style( 'xrcb-style', get_template_directory_uri() . '/admin.css');
	}
}
add_action( 'wp_enqueue_scripts', 'xrcb_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

/* redirect after login */
function admin_default_page() {
  return ( get_site_url() . '/ca/' );
}
add_filter('login_redirect', 'admin_default_page');

/* Add custom logo */
function custom_login_logo() {
	echo '<style type="text/css">
	#login h1 a { background-image: url(https://xrcb.cat/wp-content/uploads/2018/04/xrcb.png); background-size:230px 144px; width:230px; height:144px; }
	</style>';
}
add_action('login_head', 'custom_login_logo');

/* remove admin bar */
add_filter( 'show_admin_bar', '__return_false' );

/* hide admin menu */
/*function hide_menus() {
    if ( !current_user_can('manage_options') ) {
        ?>
        <style>
           #adminmenuback, #adminmenuwrap, #wp-admin-bar-wp-logo{
                display:none;
            }
        </style>
        <?php
    }
}
add_action('admin_head', 'hide_menus');*/


// CPT UI radio post type export
function xrcb_register_my_cpts_radio() {

	/**
	 * Post Type: Radios.
	 */

	$labels = array(
		"name" => __( "Radios", "xrcb" ),
		"singular_name" => __( "Radio", "xrcb" ),
	);

	$args = array(
		"label" => __( "Radios", "xrcb" ),
		"labels" => $labels,
		"description" => "Radio station",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "radio", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-controls-volumeon",
		"supports" => array( "title", "thumbnail" ),
		"taxonomies" => array( "post_tag" ),
	);

	register_post_type( "radio", $args );
}

add_action( 'init', 'xrcb_register_my_cpts_radio' );




// ACF field group export

//include_once('advanced-custom-fields/acf.php');

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_radio',
		'title' => 'Radio',
		'fields' => array (
			array (
				'key' => 'field_5a69b5692c4ce',
				'label' => 'Categoría',
				'name' => 'categoria',
				'type' => 'radio',
				'choices' => array (
					'libre' => 'libre',
					'pirata' => 'pirata',
					'pública' => 'pública',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a685b74baee2',
				'label' => 'Barrio',
				'name' => 'barrio',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'Elija el barrio' => 'Elija el barrio',
					'el Raval' => 'el Raval',
					'el Barri Gòtic' => 'el Barri Gòtic',
					'la Barceloneta' => 'la Barceloneta',
					'Sant Pere, Santa Caterina i la Ribera' => 'Sant Pere, Santa Caterina i la Ribera',
					'el Fort Pienc' => 'el Fort Pienc',
					'la Sagrada Família' => 'la Sagrada Família',
					'la Dreta de l\'Eixample' => 'la Dreta de l\'Eixample',
					'l\'Antiga Esquerra de l\'Eixample' => 'l\'Antiga Esquerra de l\'Eixample',
					'la Nova Esquerra de l\'Eixample' => 'la Nova Esquerra de l\'Eixample',
					'Sant Antoni' => 'Sant Antoni',
					'el Poble Sec' => 'el Poble Sec',
					'la Marina del Prat Vermell' => 'la Marina del Prat Vermell',
					'la Marina de Port' => 'la Marina de Port',
					'la Font de la Guatlla' => 'la Font de la Guatlla',
					'Hostafrancs' => 'Hostafrancs',
					'la Bordeta' => 'la Bordeta',
					'Sants - Badal' => 'Sants - Badal',
					'Sants' => 'Sants',
					'les Corts' => 'les Corts',
					'la Maternitat i Sant Ramon' => 'la Maternitat i Sant Ramon',
					'Pedralbes' => 'Pedralbes',
					'Vallvidrera, el Tibidabo i les Planes' => 'Vallvidrera, el Tibidabo i les Planes',
					'Sarrià' => 'Sarrià',
					'les Tres Torres' => 'les Tres Torres',
					'Sant Gervasi - la Bonanova' => 'Sant Gervasi - la Bonanova',
					'Sant Gervasi - Galvany' => 'Sant Gervasi - Galvany',
					'el Putxet i el Farró' => 'el Putxet i el Farró',
					'Vallcarca i els Penitents' => 'Vallcarca i els Penitents',
					'el Coll' => 'el Coll',
					'la Salut' => 'la Salut',
					'la Vila de Gràcia' => 'la Vila de Gràcia',
					'el Camp d\'en Grassot i Gràcia Nova' => 'el Camp d\'en Grassot i Gràcia Nova',
					'el Baix Guinardó' => 'el Baix Guinardó',
					'Can Baró' => 'Can Baró',
					'el Guinardó' => 'el Guinardó',
					'la Font d\'en Fargues' => 'la Font d\'en Fargues',
					'el Carmel' => 'el Carmel',
					'la Teixonera' => 'la Teixonera',
					'Sant Genís dels Agudells' => 'Sant Genís dels Agudells',
					'Montbau' => 'Montbau',
					'la Vall d\'Hebron' => 'la Vall d\'Hebron',
					'la Clota' => 'la Clota',
					'Horta' => 'Horta',
					'Vilapicina i la Torre Llobeta' => 'Vilapicina i la Torre Llobeta',
					'Porta' => 'Porta',
					'el Turó de la Peira' => 'el Turó de la Peira',
					'Can Peguera' => 'Can Peguera',
					'la Guineueta' => 'la Guineueta',
					'Canyelles' => 'Canyelles',
					'les Roquetes' => 'les Roquetes',
					'Verdun' => 'Verdun',
					'la Prosperitat' => 'la Prosperitat',
					'la Trinitat Nova' => 'la Trinitat Nova',
					'Torre Baró' => 'Torre Baró',
					'Ciutat Meridiana' => 'Ciutat Meridiana',
					'Vallbona' => 'Vallbona',
					'la Trinitat Vella' => 'la Trinitat Vella',
					'Baró de Viver' => 'Baró de Viver',
					'el Bon Pastor' => 'el Bon Pastor',
					'Sant Andreu' => 'Sant Andreu',
					'la Sagrera' => 'la Sagrera',
					'el Congrés i els Indians' => 'el Congrés i els Indians',
					'Navas' => 'Navas',
					'el Camp de l\'Arpa del Clot' => 'el Camp de l\'Arpa del Clot',
					'el Clot' => 'el Clot',
					'el Parc i la Llacuna del Poblenou' => 'el Parc i la Llacuna del Poblenou',
					'la Vila Olímpica del Poblenou' => 'la Vila Olímpica del Poblenou',
					'el Poblenou' => 'el Poblenou',
					'Diagonal Mar i el Front Marítim del Poblenou' => 'Diagonal Mar i el Front Marítim del Poblenou',
					'el Besòs i el Maresme' => 'el Besòs i el Maresme',
					'Provenals del Poblenou' => 'Provenals del Poblenou',
					'Sant Martí de Provenals' => 'Sant Martí de Provenals',
					'la Verneda i la Pau' => 'la Verneda i la Pau',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5a68588bef9e4',
				'label' => 'Sede',
				'name' => 'sede',
				'type' => 'radio',
				'choices' => array (
					'fija' => 'fija',
					'itinerante' => 'itinerante',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a691fa29d5db',
				'label' => 'Dirección',
				'name' => 'location',
				'type' => 'google_map',
				'center_lat' => '41.3860',
				'center_lng' => '2.1553',
				'zoom' => 13,
				'height' => 400,
			),
			array (
				'key' => 'field_5a685812ef9e2',
				'label' => 'Año de fundación',
				'name' => 'anyo_fundacion',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 1900,
				'max' => 2100,
				'step' => 1,
			),
			array (
				'key' => 'field_5a6857bcef9e1',
				'label' => 'Breve historia',
				'name' => 'historia',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5a685849ef9e3',
				'label' => 'Licencia de uso',
				'name' => 'licencia',
				'type' => 'radio',
				'choices' => array (
					'CC' => 'CC',
					'CR' => 'CR',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a685a93f9d33',
				'label' => 'Web',
				'name' => 'web',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a68631947e47',
				'label' => 'Mail',
				'name' => 'mail',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'radio',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

// add google maps apì key
function xrcb_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyAk5XwRCBSoh65Mtc5lMZYxnxmjhKZ5NfY';
	return $api;	
}
add_filter('acf/fields/google_map/api', 'xrcb_acf_google_map_api');

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
//define('ACF_EARLY_ACCESS', '5');